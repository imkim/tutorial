---
Title:   How To Host a Website Using Cloudflare and Nginx on Ubuntu 20.04
Author:  Kim
Date:    2020-04-26T03:14:52Z
LastMod:
Draft:   true
Summary: >
  In this tutorial, we'll use the Origin Certificates from Cloudflare CA
  to protect our website hosting by Nginx, and configure the Nginx to
  accept only Authenticated Origin Pulls from Cloudflare servers. The
  advantages of using this setup are that we benefit from Cloudflare's CDN
  and fast DNS resolution while ensuring that all connections pass through
  Cloudflare. This prevents any malicious requests from reaching our Nginx
  server, aka origin server.
TranslationKey: how-to-host-a-website-using-cloudflare-and-nginx-on-ubuntu-20-04
---

## Introduction

[Cloudflare][1] is a service sits between visitor and website server, acting as a reverse proxy for websites. Cloudflare provides Content Delivery Network (CDN), DDoS mitigation and distributed domain name server services.

![Figure 1, The Cloudflare service][figure-1]

[Nginx][2] is a popular web server hosting some of the largest and highest-traffic sites on the internet. It's common for organizations to use Nginx as the origin web server behind Cloudflare's.

In this tutorial, we'll use the Origin Certificates from Cloudflare CA to protect our website hosting by Nginx, and configure the Nginx to accept only Authenticated Origin Pulls from Cloudflare servers. The advantages of using this setup are that we benefit from Cloudflare's CDN and fast DNS resolution while ensuring that all connections pass through Cloudflare. This prevents any malicious requests from reaching our Nginx server, aka origin server.

# Prerequisites

To complete this tutorial, we need:

* One Ubuntu 20.04 server set up by following the [Ubuntu 20.04 initial server set up guide][3], including a sudo non-root user and a firewall.

* Nginx installed on this server, as shown in [How To Install Nginx on Ubuntu 20.04][4].

* A [Cloudflare][1] account.

* A registered domain added to the Cloudflare account that points to our Nginx server. Follow [How To Mitigate DDoS Attacks Against Your Website with Cloudflare][5] to set it up.

* An Nginx Server Block configured for the domain, which can be done by following [How To Set Up Nginx Server Block (Virtual Hosts) on Ubuntu 16.04][6].
<!-- TODO: How To Set Up Nginx Server Block (Virtual Hosts) on Ubuntu 20.04 -->

# Step 1 — Generating an Origin TLS Certificate

We can use the Cloudflare CA to generate Origin Certificate signed by Cloudflare for free. By installing this certificate in our Nginx server (the origin server) to ensuring the communication is encrypted between Cloudflare and origin server.

To generate Origin Certificate from Origin CA, please navigate to **SSL** » **Origin Server** of Cloudflare dashboard. Click on the **Create Certificate** button in the page:

![Figure 2, Create certificate in Cloudflare dashboard][figure-2]

Keep the default option **Let Cloudflare generate a private key and a CSR** selected.

![Figure 3, The options in Origin Certificate Installation dialog][figure-3]

Click on **Next** button and we will see a dialog displays **Origin Certificate** and **Private key**. We'll save both the private key and certificate into our Nginx server. Please do not click on the **OK** button for now, because the certificate and key will be lost after the dialog disappeared. We will close this dialog after we finish the operations later.

![Figure 4, The dialog displays Origin Certificate and Private key content][figure-4]

In this tutorial, we will use two directories already exist in our server to hold the certificate and key files: `/etc/ssl/certs` directory to hold the origin certificate file, and `/etc/ssl/private` to hold the private key file. These existing directories are permission set up already, which keeps things simple.

Copy the full content of the Origin Certificate from the dialog. Then type the command shown as below in the terminal logged in our Nginx server, to open `/etc/ssl/certs/<^>your_domain<^>.pem` file where we store the Origin Certificate.

```command
sudo nano /etc/ssl/certs/<^>your_domain<^>.pem
```

Paste certificate into this file. Save the file and exit the editor.

Return back to the browser. Copy full Private key content, then open `/etc/ssl/private/<^>your_domain<^>.key` file where we store the key.

```command
sudo nano /etc/ssl/private/<^>your_domain<^>.key
```

Paste the private key into this file. Save the file and exit the editor.

<$>[warning]
**Warning:** Cloudflare’s Origin CA Certificate is only trusted by Cloudflare and therefore should only be used by origin servers that are actively connected to Cloudflare. If at any point you pause or disable Cloudflare, your Origin CA certificate will throw an untrusted certificate error.
<$>

Now we have the certificate and key files saved into our server. It's time to click on the **OK** button to close the dialog. In the coming section, we'll update the Nginx configuration to use these files.

# Step 2 — Installing the Origin Certificate in Nginx

In the Step 1 section, we generated the Origin Certificate and Private key by using the Cloudflare dashboard and stored them in our origin server. Then we need to update Nginx configuration to use them to make the connections encrypted between the origin server and Cloudflare's for security.

There's a default server block created while Nginx installed. Remove it if it exists, since we've already configured a custom server block for our domain.

```command
sudo rm /etc/nginx/sites-enabled/default
```

Open our domain's Nginx configuration file:

```command
sudo nano /etc/nginx/sites-available/<^>your_domain<^>
```

The file should look like this:

```nginx
[lable /etc/nginx/sites-available/<^>your_domain<^>]
server {
    listen 80;
    listen [::]:80;

    root /var/www/<^>your_domain<^>/html;
    index index.html index.htm index.nginx-debian.html;

    server_name <^>your_domain<^> <^>www.your_domain<^>;

    location / {
        try_files $uri $uri/ =404;
    }
}
```

We'll modify Nginx configuration to:

* Listen on `80` port, aka HTTP service port, to redirect all requests to HTTPS address accordingly.

* Listen on `443` port, aka HTTPS service port, to use the certificate and private key generated in the previous section.

Let's modify the configuration file then it looks like this:

```nginx
[lable /etc/nginx/sites-available/<^>your_domain<^>]
server {
    listen 80;
    listen [::]:80;
    server_name <^>your_domain<^> <^>www.your_domain<^>;
    <^>return 302 https://$server_name$request_uri;<^>
}

<^>server {<^>
    <^># SSL configuration<^>
    <^>listen 443 ssl http2;<^>
    <^>listen [::]:443 ssl http2;<^>
    <^>ssl_certificate     /etc/ssl/certs/your_domain.pem;<^>
    <^>ssl_certificate_key /etc/ssl/private/your_domain.key;<^>

    server_name <^>your_domain<^> <^>www.your_domain<^>;

    root /var/www/your_domain/html;
    index index.html index.htm index.nginx-debian.html;

    location / {
        try_files $uri $uri/ =404;
    }
<^>}<^>
```

Save the file and exit the editor.

Next, we need to test the Nginx configuration file to ensure there's no syntax error.

```command
sudo nginx -t
```

After the test is passed, we need to restart Nginx to active the changes that we made in the Nginx configuration.

```command
sudo systemctl restart nginx
```

Now let's navigate to **SSL/TLS** » **Overview** in Cloudflare dashboard, and select **Full (strict)** as **Your SSL/TLS encryption mode**. This informs Cloudflare to always encrypt connections end-to-end which requires a trusted CA or Cloudflare Origin CA certificate.

![Figure 5, Enable SSL/TLS encryption mode as in Full (strict)][figure-5]

Now we can visit our website at `https://<^>your_domain<^>` to verify the configurations we made works fine. If it's OK, we will see the homepage, and the browser will report it as a secure site.

From now on, our website hosted by Nginx is secured by configured to use Cloudflare Origin Certificate. Every request is protected in HTTPS, except the certificate is trusted by Cloudflare's servers only, but others can still access our site by ignoring the untrusted certificate error.

In the next section, we'll set up Authenticated Origin Pulls to verify that the origin server is indeed talking to Cloudflare's servers and not some others. By doing so, Nginx will be configured to only accept requests which use a valid client certificate from Cloudflare and requests which have not passed through Cloudflare will be dropped.

# Step 3 — Setting Up Authenticated Origin Pulls

The Origin Certificate helps Cloudflare to verify that it's talking to the correct origin server. But how can we verify the origin server is actually talking to Cloudflare? The answer is, enter TLS client authentication.

In a client authenticated TLS handshake, both sides provide a certificate to be verified. The origin server is configured to only accept requests that use a valid client certificate from Cloudflare. Requests which have not passed through Cloudflare will be dropped as they will not have Cloudflare’s certificate. This means that attackers cannot circumvent Cloudflare’s security measures and directly connect to your Nginx server.

Cloudflare presents certificates signed by a CA with the following certificate:

```
-----BEGIN CERTIFICATE-----
MIIGCjCCA/KgAwIBAgIIV5G6lVbCLmEwDQYJKoZIhvcNAQENBQAwgZAxCzAJBgNV
BAYTAlVTMRkwFwYDVQQKExBDbG91ZEZsYXJlLCBJbmMuMRQwEgYDVQQLEwtPcmln
aW4gUHVsbDEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzETMBEGA1UECBMKQ2FsaWZv
cm5pYTEjMCEGA1UEAxMab3JpZ2luLXB1bGwuY2xvdWRmbGFyZS5uZXQwHhcNMTkx
MDEwMTg0NTAwWhcNMjkxMTAxMTcwMDAwWjCBkDELMAkGA1UEBhMCVVMxGTAXBgNV
BAoTEENsb3VkRmxhcmUsIEluYy4xFDASBgNVBAsTC09yaWdpbiBQdWxsMRYwFAYD
VQQHEw1TYW4gRnJhbmNpc2NvMRMwEQYDVQQIEwpDYWxpZm9ybmlhMSMwIQYDVQQD
ExpvcmlnaW4tcHVsbC5jbG91ZGZsYXJlLm5ldDCCAiIwDQYJKoZIhvcNAQEBBQAD
ggIPADCCAgoCggIBAN2y2zojYfl0bKfhp0AJBFeV+jQqbCw3sHmvEPwLmqDLqynI
42tZXR5y914ZB9ZrwbL/K5O46exd/LujJnV2b3dzcx5rtiQzso0xzljqbnbQT20e
ihx/WrF4OkZKydZzsdaJsWAPuplDH5P7J82q3re88jQdgE5hqjqFZ3clCG7lxoBw
hLaazm3NJJlUfzdk97ouRvnFGAuXd5cQVx8jYOOeU60sWqmMe4QHdOvpqB91bJoY
QSKVFjUgHeTpN8tNpKJfb9LIn3pun3bC9NKNHtRKMNX3Kl/sAPq7q/AlndvA2Kw3
Dkum2mHQUGdzVHqcOgea9BGjLK2h7SuX93zTWL02u799dr6Xkrad/WShHchfjjRn
aL35niJUDr02YJtPgxWObsrfOU63B8juLUphW/4BOjjJyAG5l9j1//aUGEi/sEe5
lqVv0P78QrxoxR+MMXiJwQab5FB8TG/ac6mRHgF9CmkX90uaRh+OC07XjTdfSKGR
PpM9hB2ZhLol/nf8qmoLdoD5HvODZuKu2+muKeVHXgw2/A6wM7OwrinxZiyBk5Hh
CvaADH7PZpU6z/zv5NU5HSvXiKtCzFuDu4/Zfi34RfHXeCUfHAb4KfNRXJwMsxUa
+4ZpSAX2G6RnGU5meuXpU5/V+DQJp/e69XyyY6RXDoMywaEFlIlXBqjRRA2pAgMB
AAGjZjBkMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8ECDAGAQH/AgECMB0GA1Ud
DgQWBBRDWUsraYuA4REzalfNVzjann3F6zAfBgNVHSMEGDAWgBRDWUsraYuA4REz
alfNVzjann3F6zANBgkqhkiG9w0BAQ0FAAOCAgEAkQ+T9nqcSlAuW/90DeYmQOW1
QhqOor5psBEGvxbNGV2hdLJY8h6QUq48BCevcMChg/L1CkznBNI40i3/6heDn3IS
zVEwXKf34pPFCACWVMZxbQjkNRTiH8iRur9EsaNQ5oXCPJkhwg2+IFyoPAAYURoX
VcI9SCDUa45clmYHJ/XYwV1icGVI8/9b2JUqklnOTa5tugwIUi5sTfipNcJXHhgz
6BKYDl0/UP0lLKbsUETXeTGDiDpxZYIgbcFrRDDkHC6BSvdWVEiH5b9mH2BON60z
0O0j8EEKTwi9jnafVtZQXP/D8yoVowdFDjXcKkOPF/1gIh9qrFR6GdoPVgB3SkLc
5ulBqZaCHm563jsvWb/kXJnlFxW+1bsO9BDD6DweBcGdNurgmH625wBXksSdD7y/
fakk8DagjbjKShYlPEFOAqEcliwjF45eabL0t27MJV61O/jHzHL3dknXeE4BDa2j
bA+JbyJeUMtU7KMsxvx82RmhqBEJJDBCJ3scVptvhDMRrtqDBW5JShxoAOcpFQGm
iYWicn46nPDjgTU0bX1ZPpTpryXbvciVL5RkVBuyX2ntcOLDPlZWgxZCBp96x07F
AnOzKgZk4RzZPNAxCXERVxajn/FLcOhglVAKo5H0ac+AitlQ0ip55D2/mf8o72tM
fVQ6VpyjEXdiIXWUq/o=
-----END CERTIFICATE-----
```

Also, we can download the certificate provided by Cloudflare from [here][7].

We'll create a file `/etc/ssl/certs/cloudflare.pem` in our Nginx server to hold this certificate.

```command
sudo nano /etc/ssl/certs/cloudflare.pem
```

Paste the certificate into the file. Then save the file and quit the editor.

Let's begin to update the Nginx configuration to use TLS authenticated origin pull. Open the domain configuration file:

```command
sudo nano /etc/nginx/sites-available/<^>your_domain<^>
```

Add `ssl_client_certificate` and `ssl_verify_client` directives as following:

```nginx
[label /etc/nginx/sites-available/<^>your_domain<^>]
. . .

server {
    # SSL configuration
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    ssl_certificate     /etc/ssl/certs/<^>your_domain<^>.pem;
    ssl_certificate_key /etc/ssl/private/<^>your_domain<^>.key;
    <^>ssl_client_certificate /etc/ssl/certs/cloudflare.pem;<^>
    <^>ssl_verify_client on;<^>

    . . .
```

Save the file and quit the editor.

Then test Nginx configuration to ensure there's no syntax error.

```command
sudo nginx -t
```

After the test is passed, we need to restart Nginx to active the changes we made.

```command
sudo systemctl restart nginx
```

Finally, turn on the **Authenticated Origin Pulls** option from the Cloudflare dashboard on page **SSL/TLS** » **Origin Server**.

![Figure 6, Turn on Authenticated Origin Pulls][figure-6]

Now we can verify if the configuration is set up properly by visit `https://<^>your_domain<^>`. If it's OK, we will see a homepage as the previous one.

To verify that our server accepts only requests signed by Cloudflare Origin CA, please turn off the Authenticated Origin Pulls for temporary and refresh the web page. There should be an error message like this:

![Figure 7, No required SSL certificate error message][figure-7]

The origin server will raise a "No required SSL certificate" error when there's a request without Cloudflare signed.

For now, we can ensure the configuration is set up. Let's go back to the Cloudflare dashboard, navigating to **SSL/TLS** » **Origin Server**, turn on the **Authenticated Origin Pulls** option again.

# Conclusion

In this tutorial, we secured our Nginx-powered website by encrypting traffic between Cloudflare and the Nginx server using an Origin CA certificate from Cloudflare. We then set up Authenticated Origin Pulls on the Nginx server to ensure that it only accepts requests from Cloudflare’s servers, preventing anyone else from directly connecting to the Nginx server.

[1]: https://www.cloudflare.com/ "Cloudflare - The Web Performance & Security Company \| Cloudflare"
[2]: https://www.nginx.com/ "NGINX \| High Performance Load Balancer, Web Server, & Reverse Proxy"
[3]: https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04 "Initial Server Setup with Ubuntu 20.04"
[4]: https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04 "How To Install Nginx on Ubuntu 20.04"
[5]: https://www.digitalocean.com/community/tutorials/how-to-mitigate-ddos-attacks-against-your-website-with-cloudflare "How To Mitigate DDoS Attacks Against Your Website with CloudFlare"
[6]: https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04 "How To Set Up Nginx Server Blocks (Virtual Hosts) on Ubuntu 16.04"
[7]: https://support.cloudflare.com/hc/en-us/article_attachments/360044928032/origin-pull-ca.pem "Cloudflare Origin Pull Certificate"
[figure-1]: figure-1.png "Figure 1, The Cloudflare service"
[figure-2]: figure-2.png "Figure 2, Create certificate in Cloudflare dashboard"
[figure-3]: figure-3.png "Figure 3, The options in Origin Certificate Installation dialog"
[figure-4]: figure-4.png "Figure 4, The dialog displays Origin Certificate and Private key content"
[figure-5]: figure-5.png "Figure 5, Enable SSL/TLS encryption mode as in Full (strict)"
[figure-6]: figure-6.png "Figure 6, Turn on Authenticated Origin Pulls"
[figure-7]: figure-7.png "Figure 7, No required SSL certificate error message"
