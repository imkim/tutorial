---
Title:   如何在 Ubuntu 20.04 上使用 Cloudflare 和 Nginx 托管网站
Author:  Kim
Date:    2020-04-26T03:14:52Z
LastMod:
Summary: >
  在本教程中，我们将使用 Cloudflare 提供的源 CA 证书保护 Nginx 搭建的网站，并将
  Nginx 配置成只接受经过身份验证的访问。通过这样的设置，我们可以在享受 Cloudflare
  的 CDN 和 DNS 快速解析所带来的好处的同时，确保所有的连接都经由 Cloudflare
  转达，以防止恶意请求直接到达我们的服务器。
Draft:          false
IsCJKLanguage:  true
TranslationKey: how-to-host-a-website-using-cloudflare-and-nginx-on-ubuntu-20-04
---

## 简介

[Cloudflare][1] 是位于网站访客与服务器之间的、作为网站反向代理的一种服务。Cloudflare 提供了：内容交付网络（CDN，Content Delivery Network）、缓解 DDoS、分布式域名服务器等多项服务内容。

![图 1：Cloudflare 服务][figure-1]

[Nginx][2] 是一种流行的 Web 服务器，承担着 Internet 上很多最大的、最繁忙的网站的托管工作。使用 Nginx 提供网络服务，同时使用 Cloudflare 作为 CDN 和 DNS 服务商，这是一种常见的网站搭建方式。

在本教程中，我们将使用 Cloudflare 提供的源 CA 证书保护 Nginx 搭建的网站，并将 Nginx 配置成只接受经过身份验证的访问。通过这样的设置，我们可以在享受 Cloudflare 的 CDN 和 DNS 快速解析所带来的好处的同时，确保所有的连接都经由 Cloudflare 转达，以防止恶意请求直接到达我们的服务器。

# 准备工作

为了完成本教程，我们需要提前准备：

* 一台安装了 Ubuntu 20.04 操作系统的服务器，该服务器上有一个拥有 `sudo` 权限的普通账户。参考《[Ubuntu 20.04 服务器初始化指南][3]》。

* 该服务器上已经安装 Nginx，参考《[如何在 Ubuntu 20.04 上安装 Nginx][4]》。

* [Cloudflare][1] 账户。

* 注册一个域名，将该域名添加到 Cloudflare 账户，使其指向 Nginx 服务器。参考《[如何使用 Cloudflare 防止针对服务器的 DDoS 攻击][5]》。

* 一个针对域名配置的 Nginx Server Block。参考《[如何在 Ubuntu 16.04 上设置 Nginx Server Block（虚拟主机）][6]》。
<!-- TODO: 《如何在 Ubuntu 20.04 上设置 Nginx Server Block（虚拟主机）》 -->

# 第 1 步——生成源证书

我们可以用 Cloudflare 源 CA 生成由 Cloudflare 签发的免费 TLS 证书（源证书）。通过将这个证书安装到我们的 Nginx 服务器（源服务器）上，可以确保在源服务器和 Cloudflare 服务器之间的通讯使用端到端加密。

要用源 CA 生成证书（源证书），请在 Cloudflare 仪表板中访问 **SSL/TLS** » **源服务器**。在该页面中点击 **创建证书** 按钮。如下图：

![图 2：在 Cloudflare 仪表板中创建证书][figure-2]

请保持默认选项 **请让 Cloudflare 生成私钥和 CSR** 被选中。

![图 3：“源证书安装”中的选项][figure-3]

点击 **下一步** 按钮，将显示一个包含 **源证书** 和 **私钥** 的对话框。请将此处显示的的源证书和私钥保存到 Nginx 服务器上。现在还不能点击 **OK** 按钮，因为点击后将关闭这个对话框，导致生成的源证书与私钥内容遗失。等完成后续操作后，我们再关闭这个对话框。

![图 4：显示源证书和私钥的对话框][figure-4]

在本教程中，我们将使用服务器上已经存在的两个目录来存放源证书和私钥文件：`/etc/ssl/certs` 目录存放源证书、`/etc/ssl/private` 目录存放私钥文件。这两个目录默认已完成访问权限的设置，直接使用它们可以简化操作。

首先，将 **源证书** 的内容复制到剪贴板。

然后，在登录到服务器的终端中输入如下命令，打开 `/etc/ssl/certs/<^>your_domain<^>.pem` 文件进行编辑：

```command
sudo nano /etc/ssl/certs/<^>your_domain<^>.pem
```

将证书内容粘贴到这个文件中。保存文件，并关闭编辑器。

返回浏览器，将 **私钥** 内容复制进剪贴板。然后用编辑器打开 `/etc/ssl/private/<^>your_domain<^>.key`：

```command
sudo nano /etc/ssl/private/<^>your_domain<^>.key
```

将密钥粘贴进去。保存文件，关闭编辑器。

<$>[warning]
**警告：** Cloudflare 源 CA 证书仅被 Cloudflare 信任，只能被直连到 Cloudflare 的源服务器使用。一旦暂停或停止使用 Cloudflare 所提供的服务，则该源证书将引发“证书不被信任错误”。
<$>

现在，我们已将证书和私钥文件保存到服务器。此时，可以在浏览器中点击 **OK** 按钮关闭对话框。接下来，我们还要更新 Nginx 的配置信息，以便使用这两个文件。

# 第 2 步——在 Nginx 中安装源证书

在上一个步骤中，我们使用 Cloudflare 仪表板生成了源证书与私钥，并已将他们保存在服务器。接下来，我们将更新 Nginx 配置信息，让我们的服务器与 Cloudflare 的服务器在通讯时使用源证书和私钥进行加密。

Nginx 在安装时会创建一个 Default Server Block。请删除它。因为我们已经为域名配置了自定义 Server Block。

```command
sudo rm /etc/nginx/sites-enabled/default
```

然后，打开我们的域名的 Nginx 配置文件：

```command
sudo nano /etc/nginx/sites-available/<^>your_domain<^>
```

该文件看起来应该是这样的：

```nginx
[label /etc/nginx/sites-available/<^>your_domain<^>]
server {
    listen 80;
    listen [::]:80;

    root /var/www/<^>your_domain<^>/html;
    index index.html index.htm index.nginx-debian.html;

    server_name <^>your_domain<^> <^>www.your_domain<^>;

    location / {
        try_files $uri $uri/ =404;
    }
}
```

我们将修改 Nginx 配置文件，以完成：

* 侦听在 `80` 端口上，并将所有访问请求转发成使用 HTTPS 安全协议（`443` 端口）。

* 侦听在 `443` 端口上，并使用在上一节中添加的源证书和私钥。

将文件修改成如下的样子：

```nginx
[lable /etc/nginx/sites-available/<^>your_domain<^>]
server {
    listen 80;
    listen [::]:80;
    server_name <^>your_domain<^> <^>www.your_domain<^>;
    <^>return 302 https://$server_name$request_uri;<^>
}

<^>server {<^>
    <^># SSL configuration<^>
    <^>listen 443 ssl http2;<^>
    <^>listen [::]:443 ssl http2;<^>
    <^>ssl_certificate     /etc/ssl/certs/your_domain.pem;<^>
    <^>ssl_certificate_key /etc/ssl/private/your_domain.key;<^>

    server_name <^>your_domain<^> <^>www.your_domain<^>;

    root /var/www/your_domain/html;
    index index.html index.htm index.nginx-debian.html;

    location / {
        try_files $uri $uri/ =404;
    }
<^>}<^>
```

保存文件，并退出编辑器。

接下来，测试 Nginx 配置文件，以确保配置中没有语法错误：

```command
sudo nginx -t
```

通过测试后，为了启用我们所做的修改，需要重新启动 Nginx：

```command
sudo systemctl restart nginx
```

现在，我们再到 Cloudflare 仪表板的 **SSL/TLS** » **概述** 中，将 **您的 SSL/TLS 加密模式** 修改为 **完全（严格）**。这将通知 Cloudflare 总是将 Cloudflare 和原服务器之间的连接加密保护起来。

![图 5：在 Cloudflare 仪表板中开启 SSL 加密模式][figure-5]

现在，通过网址 `https://<^>your_domain<^>` 浏览我们的网站，验证以上设置是否正确。如果一切顺利，可以看到网站的主页，同时，浏览器会报告该网站是安全的。

在下一个步骤中，我们将设置经过身份验证的源服务器拉取，以验证原始服务器确实是与 Cloudflare （而非其它服务器）进行通信。为了达到这个目的，Nginx 将会被配置成只接受使用了有效 Cloudflare 客户证书的访问请求，来自其它地方的访问请求将被丢弃不予处理。

# 第 3 步——设置经过身份验证的源服务器拉取

源证书可以帮助 Cloudflare 验证是否正在与正确的源务器进行通信。但怎样才能让源服务器验证是否正在与 Cloudflare 通信呢？答案是：输入 TLS 客户端身份验证。

在客户身份端验证的 TLS 握手中，双方都要提供证书以完成验证。源服务器被配置成只接受使用有效 Cloudflare 客户证书的访问请求。未通过 Cloudflare 的访问请求将被丢弃，因为它们没有 Cloudflare 的证书，更无法通过验证过程中的加解密校验。也就是说，攻击者不能绕过 Cloudflare 的安全措施直接连接到我们的 Nginx 服务器。

Cloudflare 提供了由 CA 签名的证书，如下：

```
-----BEGIN CERTIFICATE-----
MIIGCjCCA/KgAwIBAgIIV5G6lVbCLmEwDQYJKoZIhvcNAQENBQAwgZAxCzAJBgNV
BAYTAlVTMRkwFwYDVQQKExBDbG91ZEZsYXJlLCBJbmMuMRQwEgYDVQQLEwtPcmln
aW4gUHVsbDEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzETMBEGA1UECBMKQ2FsaWZv
cm5pYTEjMCEGA1UEAxMab3JpZ2luLXB1bGwuY2xvdWRmbGFyZS5uZXQwHhcNMTkx
MDEwMTg0NTAwWhcNMjkxMTAxMTcwMDAwWjCBkDELMAkGA1UEBhMCVVMxGTAXBgNV
BAoTEENsb3VkRmxhcmUsIEluYy4xFDASBgNVBAsTC09yaWdpbiBQdWxsMRYwFAYD
VQQHEw1TYW4gRnJhbmNpc2NvMRMwEQYDVQQIEwpDYWxpZm9ybmlhMSMwIQYDVQQD
ExpvcmlnaW4tcHVsbC5jbG91ZGZsYXJlLm5ldDCCAiIwDQYJKoZIhvcNAQEBBQAD
ggIPADCCAgoCggIBAN2y2zojYfl0bKfhp0AJBFeV+jQqbCw3sHmvEPwLmqDLqynI
42tZXR5y914ZB9ZrwbL/K5O46exd/LujJnV2b3dzcx5rtiQzso0xzljqbnbQT20e
ihx/WrF4OkZKydZzsdaJsWAPuplDH5P7J82q3re88jQdgE5hqjqFZ3clCG7lxoBw
hLaazm3NJJlUfzdk97ouRvnFGAuXd5cQVx8jYOOeU60sWqmMe4QHdOvpqB91bJoY
QSKVFjUgHeTpN8tNpKJfb9LIn3pun3bC9NKNHtRKMNX3Kl/sAPq7q/AlndvA2Kw3
Dkum2mHQUGdzVHqcOgea9BGjLK2h7SuX93zTWL02u799dr6Xkrad/WShHchfjjRn
aL35niJUDr02YJtPgxWObsrfOU63B8juLUphW/4BOjjJyAG5l9j1//aUGEi/sEe5
lqVv0P78QrxoxR+MMXiJwQab5FB8TG/ac6mRHgF9CmkX90uaRh+OC07XjTdfSKGR
PpM9hB2ZhLol/nf8qmoLdoD5HvODZuKu2+muKeVHXgw2/A6wM7OwrinxZiyBk5Hh
CvaADH7PZpU6z/zv5NU5HSvXiKtCzFuDu4/Zfi34RfHXeCUfHAb4KfNRXJwMsxUa
+4ZpSAX2G6RnGU5meuXpU5/V+DQJp/e69XyyY6RXDoMywaEFlIlXBqjRRA2pAgMB
AAGjZjBkMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMBAf8ECDAGAQH/AgECMB0GA1Ud
DgQWBBRDWUsraYuA4REzalfNVzjann3F6zAfBgNVHSMEGDAWgBRDWUsraYuA4REz
alfNVzjann3F6zANBgkqhkiG9w0BAQ0FAAOCAgEAkQ+T9nqcSlAuW/90DeYmQOW1
QhqOor5psBEGvxbNGV2hdLJY8h6QUq48BCevcMChg/L1CkznBNI40i3/6heDn3IS
zVEwXKf34pPFCACWVMZxbQjkNRTiH8iRur9EsaNQ5oXCPJkhwg2+IFyoPAAYURoX
VcI9SCDUa45clmYHJ/XYwV1icGVI8/9b2JUqklnOTa5tugwIUi5sTfipNcJXHhgz
6BKYDl0/UP0lLKbsUETXeTGDiDpxZYIgbcFrRDDkHC6BSvdWVEiH5b9mH2BON60z
0O0j8EEKTwi9jnafVtZQXP/D8yoVowdFDjXcKkOPF/1gIh9qrFR6GdoPVgB3SkLc
5ulBqZaCHm563jsvWb/kXJnlFxW+1bsO9BDD6DweBcGdNurgmH625wBXksSdD7y/
fakk8DagjbjKShYlPEFOAqEcliwjF45eabL0t27MJV61O/jHzHL3dknXeE4BDa2j
bA+JbyJeUMtU7KMsxvx82RmhqBEJJDBCJ3scVptvhDMRrtqDBW5JShxoAOcpFQGm
iYWicn46nPDjgTU0bX1ZPpTpryXbvciVL5RkVBuyX2ntcOLDPlZWgxZCBp96x07F
AnOzKgZk4RzZPNAxCXERVxajn/FLcOhglVAKo5H0ac+AitlQ0ip55D2/mf8o72tM
fVQ6VpyjEXdiIXWUq/o=
-----END CERTIFICATE-----
```

也可以通过 Cloudflare 提供的 [这个网址][7] 直接下载该证书。

使用如下命令在我们的 Nginx 服务器上新建一个用于保存 Cloudflare 证书的文件 `/etc/ssl/certs/cloudflare.pem`：

```command
sudo nano /etc/ssl/certs/cloudflare.pem
```

将证书粘贴进文件，然后保存文件，最后退出编辑器。

现在，更新 Nginx 配置信息，以便开始使用 TLS 身份验证的源服务器拉取。打开域名的配置文件：

```command
sudo nano /etc/nginx/sites-available/<^>your_domain<^>
```

参考下面的示例，添加 `ssl_client_certificate` 和 `ssl_verify_client` 指令：

```nginx
[label /etc/nginx/sites-available/<^>your_domain<^>]
. . .

server {
    # SSL configuration
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    ssl_certificate     /etc/ssl/certs/<^>your_domain<^>.pem;
    ssl_certificate_key /etc/ssl/private/<^>your_domain<^>.key;
    <^>ssl_client_certificate /etc/ssl/certs/cloudflare.pem;<^>
    <^>ssl_verify_client on;<^>

    . . .
```

保存文件，并退出编辑器。

接下来，测试 Nginx 配置文件，以确保配置中没有语法错误：

```command
sudo nginx -t
```

通过测试后，为了启用我们所做的修改，需要重新启动 Nginx：

```command
sudo systemctl restart nginx
```

最后，在 Cloudflare 仪表板的 **SSL/TLS** » **源服务器** 页面中打开 **经过身份验证的源服务器拉取**。

![图 6：开启“经过身份验证的源服务器拉取”][figure-6]

现在通过浏览器访问 `https://<^>your_domain<^>` 可以验证设置是否正确。跟之前一样，在浏览器中可以看到网站的首页。

为了验证服务器只接受被 Cloudflare CA 签名过的访问，请暂时关闭“经过身份验证的源服务器拉取”开关，然后刷新网页。此时，应该看到如下错误信息。

![图 7：无法提供 SSL 证书的错误信息][figure-7]

源服务器在收到未使用 Cloudflare CA 签名的访问请求时，会引发一个“无法提供 SSL 证书”的错误。

至此，可以确定设置配置成功。重新回到 Cloudflare 仪表板的 **SSL/TLS** » **源服务器** 页面，将 **经过身份验证的源服务器拉取** 选项重新打开。

# 小结

在这个教程中，我们使用 Cloudflare 颁发的源证书配置了 Nginx 服务器，将 Cloudflare 到 Nginx 服务器的通信加密保护；然后，我们在 Nginx 服务器上设置了“经过身份验证的源服务器拉取”，让这台服务器只接受来自 Cloudflare 服务器的访问请求，以避免其他人可以直接访问这台 Nginx 服务器。

[1]: https://www.cloudflare.com/ "Cloudflare - The Web Performance & Security Company \| Cloudflare"
[2]: https://www.nginx.com/ "NGINX \| High Performance Load Balancer, Web Server, & Reverse Proxy"
[3]: https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04 "Initial Server Setup with Ubuntu 20.04"
[4]: https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04 "How To Install Nginx on Ubuntu 20.04"
[5]: https://www.digitalocean.com/community/tutorials/how-to-mitigate-ddos-attacks-against-your-website-with-cloudflare "How To Mitigate DDoS Attacks Against Your Website with CloudFlare"
[6]: https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-server-blocks-virtual-hosts-on-ubuntu-16-04 "How To Set Up Nginx Server Blocks (Virtual Hosts) on Ubuntu 16.04"
[7]: https://support.cloudflare.com/hc/zh-cn/article_attachments/360044928032/origin-pull-ca.pem "Cloudflare Origin Pull 证书"
[figure-1]: figure-1.png "图 1：Cloudflare 服务"
[figure-2]: figure-2.png "图 2：在 Cloudflare 仪表板中创建证书"
[figure-3]: figure-3.png "图 3：“源证书安装”中的选项"
[figure-4]: figure-4.png "图 4：显示源证书和私钥的对话框"
[figure-5]: figure-5.png "图 5：在 Cloudflare 仪表板中开启 SSL 加密模式"
[figure-6]: figure-6.png "图 6：开启“经过身份验证的源服务器拉取”"
[figure-7]: figure-7.png "图 7：无法提供 SSL 证书的错误信息"
