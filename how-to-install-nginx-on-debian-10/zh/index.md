---
Title:   如何在 Debian 10 上安装 Nginx
Date:    2020-03-27T09:06:51Z
LastMod: 2020-04-23T19:25:16Z
Author:  Kim
Draft:   false
IsCJKLanguage:  true
TranslationKey: how-to-install-nginx-on-debian-10
Summary: >
  Nginx 是最受欢迎的 Web 服务器之一。被用于托管 Internet 上大型、大流量网站。在多数情况下，Nginx 会比 Apache 更节省系统资源，被普遍应用在 Web 服务器、反向代理等场景中。

  在本指南中，我们将讨论如何在安装了 Debian 10 操作系统的服务器上安装 Nginx。
---

## 简介

[Nginx][1] 是最受欢迎的 Web 服务器之一。被用于托管 Internet 上大型、大流量网站。在多数情况下，Nginx 会比 Apache 更节省系统资源，被普遍应用在 Web 服务器、反向代理等场景中。

在本指南中，我们将讨论如何在安装了 Debian 10 操作系统的服务器上安装 Nginx。

# 准备工作

在开始本指南之前，我们需要一台拥有 sudo 权限的普通账户（非 root 账户）的服务器，同时该服务器也需要一个可以正常工作的防火墙。如果我们的服务器现在还不具备这些先决条件，请参考《 [Debian 10 服务器初始化指南][2] 》一文。

在准备好这个普通账户后，使用这个账户登录到服务器，然后开始跟着本指南一步一步进行操作。

# 第 1 步——安装 Nginx

Debian 软件仓库中已经包含 Nginx 软件包，因此可以直接使用 `apt` 软件包系统从软件仓库中安装 Nginx。

首次使用 `apt` 软件包系统时，需要先用以下命令更新本地的软件包索引，以便可以安装最新版本的软件包：

```command
sudo apt update
```

索引更新完成后，就可以使用如下命令安装 `nginx` 软件包了：

```command
sudo apt install nginx
```

当屏幕出现确认提示信息时，在键盘上敲 `ENTER` 键。此后，`apt` 会完成 Nginx 软件包以及运行 Nginx 时所依赖的其它相关软件包的安装。

# 第 2 步——配置防火墙

开始测试 Nginx 是否可以正常工作之前，需要先配置防火墙，让 Web 服务可以被访问到。

输入下面这条命令可以查看 `ufw` 所支持的应用程序防火墙规则：

```command
sudo ufw app list
```

你可以看到类似如下的列表：

```
[secondary_label Output]
Available applications:
...
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
...
```

从上面这个列表中，我们可以发现三条与 Nginx 相关的防火墙规则：

* **Nginx Full**：这个规则将在服务器上开放从外部访问 `80` 端口（普通、未加密的流量）和 `443` 端口（采用 TLS/SSL 加密的流量）。

* **Nginx HTTP**：这个规则将在服务器上开放从外部访问 `80` 端口（普通、未加密的流量）

* **Nginx HTTPS**：这个规则将在服务器上开放从外部访问 `443` 端口（采用 TLS/SSL 加密的流量）

通常情况下，建议根据实际情况启用最严格（开放端口最少的） Profile，不使用的端口不对外开放访问许可。鉴于在本指南中我们尚未讨论如何配置 SSL，因此只允许外部访问运行在 `80` 端口上的 HTTP 服务。

使用如下命令启用防火墙规则：

```command
sudo ufw allow 'Nginx HTTP'
```

然后使用如下命令确认防火墙是否启用成功：

```command
sudo ufw status
```

若启用成功，你将在报告中看到 HTTP 流量被启用的信息（如下）：

```
[secondary_label Output]
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere
Nginx HTTP                 ALLOW       Anywhere
OpenSSH (v6)               ALLOW       Anywhere (v6)
Nginx HTTP (v6)            ALLOW       Anywhere (v6)
```

# 第 3 步——检查 Web 服务器

安装成功后，Debian 10 会自动运行 Nginx。此时，Web 服务器正在后台运行。

Debian 10 使用 `systemd` 系统控制后台运行的服务程序。可以通过如下命令检查 Nginx 是否正在后台运行：

```command
systemctl status nginx
```

```
[secondary_label Output]
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2019-07-03 12:52:54 UTC; 4min 23s ago
     Docs: man:nginx(8)
 Main PID: 3942 (nginx)
    Tasks: 3 (limit: 4719)
   Memory: 6.1M
   CGroup: /system.slice/nginx.service
           ├─3942 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
           ├─3943 nginx: worker process
           └─3944 nginx: worker process
```

如上所示，表示 Nginx 服务已启动成功。当然，最好的检查方法肯定是直接访问由 Nginx 所托管的网页。

我们可以通过访问服务器的 IP 地址来确定 Nginx 是否运行正常。如果我们在浏览器中可以看到了 Nginx 初始页面，则说明 Nginx 运行正常。如果不知道服务器的 IP 地址，在终端中试试下面这个命令：

```command
ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'
```

该命令会返回几行文字，每行都是一个 IP 地址。可以在浏览器中逐一尝试。

在浏览器的地址栏中输入我们找到的服务器 IP 地址：

```
http://<^>your_server_ip<^>
```

然后可以看到如下的 Nginx 初始页面：

![图 1：Nginx 初始页面][figure-1]

这个初始页面是在 Nginx 安装时自动部署的，可以用来检测服务是否运行正常。

# 第 4 步——管理 Nginx 进程

阅读到这里，说明我们已启动了 Web 服务。现在让我们来学习一些基本的管理命令。

使用如下命令停止 Web 服务：

```command
sudo systemctl stop nginx
```

使用如下命令开启 Web 服务（当 Web 服务处于“停止”状态时，需要“开启”它）：

```command
sudo systemctl start nginx
```

使用如下命令重新开启 Web 服务（无论 Web 服务是否处于“停止”状态，强制其立即“停止”并再次“开启”）：

```command
sudo systemctl restart nginx
```

如果我们修改了 Nginx 的配置文件，可以通过下面这条命令在不切断网络连接的情况下启用新的配置。（注：如前所示的 `stop` 和 `restart` 命令会立即切断正在访问服务器的访客，造成访客浏览器报错。而 `reload` 命令却可以避免这种不友好的客户体验。）

```command
sudo systemctl reload nginx
```

默认情况下，Nginx 会在服务器开机时自动启动。若不希望如此，可以使用如下命令禁止 Nginx 在开始时的自动启动：

```command
sudo systemctl disable nginx
```

若希望 Nginx 跟随操作系统自动启动，可使用如下命令：

```command
sudo systemctl enable nginx
```

# 第 5 步——配置 Server Block

Nginx 支持在一台服务器上同时托管多个网站（多个域名）。这些网站通过 **Server Block** 各自独立进行配置（这个特性类似于 Apache 中的虚拟主机）。我们将使用 **your_domain** 来指代本指南中所使用的域名。关于如何在 DigitalOcean 中配置域名，参见《[DigitalOcean DNS 简介][3]》一文。

<!-- TODO: 下面这个段落的文字表达仍不够流畅，需要更好地组织语言。 -->
Debian 10 的 Nginx 软件包默认启用了一个 Server Block，该 Server Block 允许访客浏览保存在服务器 `/var/www/html` 目录中的文档。由于只配置了一个网站，因此，在需要开启多个网站时，这样的配置就显得毫无用处。同时，虽然我们可以将网站内容保存到 `/var/www/html` 目录中，但这样会破坏 Nginx 软件包的结构。作为更好的方案，我们将在 `/var/www` 路径下创建名为 **your_domain** 的目录结构，保持原有 `/var/www/html` 目录中的内容保持不变。这样的好处是：当有访客访问没有配置的网站时，Nginx 可以向访客提供默认的初始网页。

请使用如下命令在服务器上创建 **your_domain** 的目录结构（使用 `-p` 参数可以根据服务器的文件组织结构自动创建相关目录）：

```command
sudo mkdir -p /var/www/<^>your_domain<^>/html
```

然后，使用以下命令为新创建的目录分配用户权限。其中 `$USER` 环境变量是我们当前登录服务器的用户：

```command
sudo chown -R $USER:$USER /var/www/<^>your_domain<^>/html
```

如果没有修改过 `umask`，此时新创建的目录已经分配好了访问权限。若不能确定，可使用下面这个命令确保目录的访问权限被正确配置：

```command
sudo chmod -R 755 /var/www/<^>your_domain<^>
```

接着，使用 `nano` （或者其它习惯的编辑器）创建一个简易 `index.html` 页面：

```command
nano /var/www/<^>your_domain<^>/html/index.html
```

在编辑器中输入以下简易 HTML：

```html
[label /var/www/<^>your_domain<^>/html/index.html]
<html>
    <head>
        <title>欢迎来到 <^>your_domain<^></title>
    </head>
    <body>
        <h1>成功！Nginx 服务器已经针对 <em><^>your_domain<^></em> 配置成功。</h1>
        <p>这是一个样例页面。</p>
    </body>
</html>
```

完成后，保存并关闭文件。

为了让这个简易页面可以被 Nginx 使用，还需要创建一个 Server Block，并在这个 Server Block 中指明网站的根目录。除了直接修改 Nginx 默认的配置文件外，我们可以创建另外一个名为 `/etc/nginx/sites-available/<^>your_domain<^>` 的配置文件：

```command
sudo nano /etc/nginx/sites-available/<^>your_domain<^>
```

将下面的配置内容粘贴到新建的配置文件中。这个配置是根据默认配置修改得来的，并用我们新建的目录和域名做了相应替换：

```nginx
[label /etc/nginx/sites-available/your_domain]
server {
        listen 80;
        listen [::]:80;

        root /var/www/<^>your_domain<^>/html;
        index index.html index.htm index.nginx-debian.html;

        server_name <^>your_domain<^> www.<^>your_domain<^>;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

注意：我们已修改了 `root` 配置项的内容，将其指向我们新建的目录；同时也将 `server_name` 替换为我们的域名。

接下来，通过在 `sites-enabled` 目录中创建一个指向这个 Server Block 的符号链接。这样，Nginx 才可以在启动时读取到这些配置。

```command
sudo ln -s /etc/nginx/sites-available/<^>your_domain<^> /etc/nginx/sites-enabled/
```

现在我们的配置中有了两个 Server Block。当网站访客浏览服务器上的 Web 服务时，Nginx 会根据其中的 `listen` 和 `server_name` 决定到底应该向访客提供哪个网站的内容。（`listen` 和 `server_name` 在 Nginx 配置文件中被称为“指令”。更多关于 Nginx 指令的说明，参见 [这份文档][4] 。）

* `<^>your_domain<^>`：当访客访问 `<^>your_domain<^>` 和 `www.<^>your_domain<^>` 网站时，将使用这个 Server Block。

* `<^>default<^>`：当访客访问监听在 `80` 端口的 HTTP 服务，且被访问的域名不能在其它 Server Block 中找到匹配的域名，此时将使用这个 Server Block。

为避免在添加服务器名称时可能产生的 Hush Bucket 内存问题，应该调整 `/etc/nginx/nginx.conf` 文件中的相应参数。用编辑器打开文件：

```command
sudo nano /etc/nginx/nginx.conf
```

找到 `server_names_hash_bucket_size` 指令，删除指令前的 `#` 符号，使这个指令生效：

```nginx
[lable /etc/nginx/nginx.conf]
...
http {
    ...
    server_names_hash_bucket_size 64;
    ...
}
...
```

完成调整后，保存并关闭文件。

下面，我们检查一下 Nginx 的配置文件中是否存在错误：

```command
sudo nginx -t
```

如果没有错误，将看到如下内容：

```
[secondary_label Output]
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

完成测试后，需要重新启动 Nginx 使修改过的配置生效。

```command
sudo systemctl restart nginx
```

至此，Nginx 应该正在托管我们域名的网站。可以通过在浏览器中访问网址 `http://<^>your_domain<^>` 看到如下的页面：

![图 2：Nginx 的 Server Block][figure-2]

# 第 6 步——熟悉重要的 Nginx 文件和目录

我们现在已经学会了如何管理 Nginx 服务，下面再多花几分钟时间熟悉几个重要的目录和文件。

## 网站内容

* `/var/www/html`：真正的 Web 网站内容被存放在 `/var/www/html` 目录里。默认情况下，该目录包含在前面看到的哪个初始页面。可通过修改 Nginx 配置文件修改此路径。

## 配置文件

* `/etc/nginx`：Nginx 配置目录。所有 Nginx 的配置文件都被存放在这里。

* `/etc/nginx/nginx.conf`：Nginx 主配置文件。可通过修改 Nginx 全局配置修改这个路径。

* `/etc/nginx/sites-available/`：该目录用于存放 Server Block。每个网站一个 Server Block。存放在此处的配置文件并不会被 Nginx 使用，除非将文件链接到 `sites-enabled` 目录里。通常情况下，我们把全部 Server Block 配置文件存放在这个地方，然后将其中需要启用的配置文件链接到 `sites-enabled` 目录。

* `/etc/nginx/sites-enabled/`：该目录用于存放需要启用的 Server Block。每个网站一个 Server Block。通常情况下，这里的文件都是 `sites-available` 目录中某个 Server Block 配置文件的链接。

* `/etc/nginx/snippets`：该目录包含配置片段。这些配置片段可以在 Nginx 配置中被引入。当我们有一些配置需要在多个地方被重复使用时，可以将这些配置做成配置片段存放在这里，以此方便管理配置。

## 日志文件

* `/var/log/nginx/access.log`：除非对 Nginx 的配置进行了特别调整，否则每次访客对 Web 服务的访问请求都会被记录在这个日志文件中。

* `/var/log/nginx/error.log`：Nginx 在运行过程中发生的错误都会被记录在这个文件中。

# 总结

至此，我们已完成了 Web 服务器的安装，可以用这个服务器向网站访客提供内容丰富、技术时髦的网站。

[1]: https://nginx.org/
[2]: https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10
[3]: https://www.digitalocean.com/docs/networking/dns/
[4]: https://www.digitalocean.com/community/tutorials/understanding-nginx-server-and-location-block-selection-algorithms
[figure-1]: default_page.png "Nginx 初始页面"
[figure-2]: nginx_serverblock.png "Nginx 的 Server Block"
